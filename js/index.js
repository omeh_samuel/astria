

let checkPass=()=>{
    let x = document.getElementById('passwd');
    if (x.type==='password'){
        x.type='text';
    } else{
        x.type='password';
    }
};




///chartjs

var color = Chart.helpers.color;
function createConfig(legendPosition, colorName) {
    return {
        type: 'line',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June'],
            datasets: [{
                label: 'My First dataset',
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor()
                ],
                backgroundColor: color(window.chartColors[colorName]).alpha(0.5).rgbString(),
                borderColor: window.chartColors[colorName],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: legendPosition,
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            },
            title: {
                display: true,
                text: 'Legend Position: ' + legendPosition
            }
        }
    };
}

window.onload = function() {
    [
    {
        id: 'chart-legend-left',
        legendPosition: 'center',
        color: 'red'
    }].forEach(function(details) {
        var ctx = document.getElementById(details.id).getContext('2d');
        var config = createConfig(details.legendPosition, details.color);
        new Chart(ctx, config);
    });
};